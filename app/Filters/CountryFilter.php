<?php

namespace App\Filters;

use Closure;

class CountryFilter
{
    public function handle($query, Closure $next)
    {
        if(request(('country'))){
            $query->where('country', request('country'));
        }
        
        return $next($query);
    }
}
