<?php

namespace App\Filters;

use Closure;

class RoleFilter
{
    public function handle($query, Closure $next)
    {
        if(request(('role'))){
            $query->where('role', request('role'));
            // return redirect('https:://google.com');
        }
        
        return $next($query);
    }
}
