<?php

use App\Models\User;
use App\Filters\RoleFilter;
use App\Filters\CountryFilter;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('email-test', function(){
  
    $details['email'] = 'your_email@gmail.com';
  
    dispatch(new App\Jobs\SendEmailJob($details));
  
    dd('done');
});

Route::get('user', function(){
    $users = app(Pipeline::class)->send(User::query())->through([
        RoleFilter::class,
        CountryFilter::class,
    ])
    ->thenReturn()
    ->orderByDesc('id')
    ->paginate(20);

    return view('user', compact(
        'users'
    ));
});
